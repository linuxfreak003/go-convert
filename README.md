## Go-Convert

This project is meant to be a library/service for converting units

Ideally I would like to be able to convert between any units possible.

The conversions are set up as edges of a graph. A [BFS](https://en.wikipedia.org/wiki/Breadth-first_search) is used to find the shortest path of conversion.

