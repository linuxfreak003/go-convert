package main

import (
	"fmt"
	"net"

	convert "gitlab.com/linuxfreak003/go-convert/conversion"
	pb "gitlab.com/linuxfreak003/go-convert/pb"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	doServe()
}

func doServe() {
	s := grpc.NewServer()
	host := "127.0.0.1"
	port := 50051
	addr := fmt.Sprintf("%s:%d", host, port)
	log.Infof("Listening on %s", addr)

	lis, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	pb.RegisterConvertServer(s, &convert.Server{})
	// Register reflection service on gRPC server.
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
