package convert

import (
	"fmt"

	pb "gitlab.com/linuxfreak003/go-convert/pb"
)

func (g *Graph) Convert(value float64, from, to pb.Unit) (float64, error) {
	path := g.BFSPath(from, to)
	if path == nil {
		return 0, fmt.Errorf("could not convert: no available path")
	}

	for _, e := range path {
		value *= e.Weight
	}
	return value, nil
}

func (g *Graph) Index(edge *Edge) int {
	for i, e := range g.Edges {
		if e.From == edge.From && e.To == edge.To {
			return i
		}
	}
	return -1
}

func (g *Graph) CreateEdge(e *Edge) (*Edge, error) {
	if g.Index(e) >= 0 {
		return nil, fmt.Errorf("could not create: edge already exists")

	}
	g.Edges = append(g.Edges, e)
	return e, nil
}

func (g *Graph) ListEdges(e *Edge) ([]*Edge, error) {
	return g.Edges, nil
}

func (g *Graph) UpdateEdge(e *Edge) (*Edge, error) {
	if i := g.Index(e); i >= 0 {
		g.Edges[i] = e
		return e, nil
	}
	return nil, fmt.Errorf("could not update: edge does not exists")
}

func (g *Graph) DeleteEdge(e *Edge) (*Edge, error) {
	if i := g.Index(e); i >= 0 {
		g.Edges = append(g.Edges[:i], g.Edges[i+1:]...)
		return e, nil
	}
	return nil, fmt.Errorf("could not delete: edge does not exists")
}

func (g *Graph) GetEdge(e *Edge) (*Edge, error) {
	if i := g.Index(e); i >= 0 {
		return g.Edges[i], nil
	}
	return nil, fmt.Errorf("could not get: edge does not exists")
}

func (g *Graph) BFSPath(start, end pb.Unit) []*Edge {
	queue := &Queue{data: []pb.Unit{start}}
	visited := make(map[pb.Unit]struct{})
	edges := make([]*Edge, 0)

	for !queue.Empty() {
		n := queue.Pop()
		visited[n] = struct{}{}

		if n == end {
			path := make([]*Edge, 0)
			next := end
			for edge := range reverse(edges) {
				if edge.To == next {
					path = append([]*Edge{edge}, path...)
					next = edge.From
				}
			}
			return path
		}
		for _, e := range g.Edges {
			if e.From == n {
				if _, v := visited[e.To]; !v {
					queue.Add(e.To)
					edges = append(edges, e)
				}
			}
		}
	}
	return nil
}

func CreateEdge(from, to pb.Unit, weight float64) *Edge {
	return &Edge{From: from, To: to, Weight: weight}
}

func NewGraph() *Graph {
	edges := []*Edge{
		CreateEdge(pb.Unit_INCH, pb.Unit_FOOT, 1.0/12.0),
		CreateEdge(pb.Unit_FOOT, pb.Unit_INCH, 12.0),
		CreateEdge(pb.Unit_FOOT, pb.Unit_YARD, 1.0/3.0),
		CreateEdge(pb.Unit_YARD, pb.Unit_FOOT, 3.0),
		CreateEdge(pb.Unit_YARD, pb.Unit_MILE, 1.0/1760.0),
		CreateEdge(pb.Unit_MILE, pb.Unit_YARD, 1760.0),
		CreateEdge(pb.Unit_INCH, pb.Unit_MILLIMETER, 25.4),
		CreateEdge(pb.Unit_YARD, pb.Unit_METER, 0.91),
		CreateEdge(pb.Unit_METER, pb.Unit_YARD, 1.0/0.91),
		CreateEdge(pb.Unit_MILE, pb.Unit_KILOMETER, 1.60934),
		CreateEdge(pb.Unit_KILOMETER, pb.Unit_MILE, 0.62),
		CreateEdge(pb.Unit_KILOMETER, pb.Unit_METER, 1000),
		CreateEdge(pb.Unit_METER, pb.Unit_KILOMETER, 1.0/1000.0),
		CreateEdge(pb.Unit_METER, pb.Unit_CENTIMETER, 100.0),
		CreateEdge(pb.Unit_CENTIMETER, pb.Unit_METER, 1.0/100.0),
		CreateEdge(pb.Unit_CENTIMETER, pb.Unit_MILLIMETER, 10.0),
		CreateEdge(pb.Unit_MILLIMETER, pb.Unit_CENTIMETER, 1.0/10.0),
	}
	return &Graph{Edges: edges}
}

type Graph struct {
	Edges []*Edge
}

type Edge struct {
	From   pb.Unit
	To     pb.Unit
	Weight float64
}

func reverse(lst []*Edge) chan *Edge {
	ret := make(chan *Edge)
	go func() {
		for i, _ := range lst {
			ret <- lst[len(lst)-1-i]
		}
		close(ret)
	}()
	return ret
}

type Queue struct {
	data []pb.Unit
}

func (q *Queue) Empty() bool {
	return len(q.data) <= 0
}

func (q *Queue) Add(n pb.Unit) {
	q.data = append(q.data, n)
}

func (q *Queue) Pop() pb.Unit {
	n := q.data[0]
	q.data = q.data[1:]
	return n
}
