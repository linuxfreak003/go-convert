package convert_test

import (
	"fmt"
	"net"
	"testing"

	"golang.org/x/net/context"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	convert "gitlab.com/linuxfreak003/go-convert/conversion"
	pb "gitlab.com/linuxfreak003/go-convert/pb"
	"google.golang.org/grpc"
)

func TestConvert(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Convert Suite")
}

var (
	host   string
	port   int
	client pb.ConvertClient
)

var _ = BeforeSuite(func() {
	host = "127.0.0.1"
	port = 50052
	testServer := grpc.NewServer()
	addr := fmt.Sprintf("%s:%d", host, port)
	listener, err := net.Listen("tcp", addr)
	Expect(err).To(BeNil())
	pb.RegisterConvertServer(testServer, convert.NewServer())
	go testServer.Serve(listener)

	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	Expect(err).To(BeNil())

	client = pb.NewConvertClient(conn)

})

var _ = Describe("Convert", func() {
	It("Converts to the correct value", func() {
		res, err := client.Convert(context.Background(), &pb.ConvertRequest{
			Value:   1.0,
			UnitIn:  pb.Unit_FOOT,
			UnitOut: pb.Unit_INCH,
		})
		Expect(err).To(BeNil())
		Expect(res.Value).To(BeEquivalentTo(12))
		Expect(res.Unit).To(Equal(pb.Unit_INCH))
	})
	It("converts 1 mile to 63360 inches", func() {
		res, err := client.Convert(context.Background(), &pb.ConvertRequest{
			Value:   1.0,
			UnitIn:  pb.Unit_MILE,
			UnitOut: pb.Unit_INCH,
		})
		Expect(err).To(BeNil())
		Expect(res.Value).To(BeEquivalentTo(63360))
		Expect(res.Unit).To(Equal(pb.Unit_INCH))
	})
	It("converts 1 mile to 160934 centimeter", func() {
		res, err := client.Convert(context.Background(), &pb.ConvertRequest{
			Value:   1.0,
			UnitIn:  pb.Unit_MILE,
			UnitOut: pb.Unit_CENTIMETER,
		})
		Expect(err).To(BeNil())
		Expect(res.Value).To(BeEquivalentTo(160934))
		Expect(res.Unit).To(Equal(pb.Unit_CENTIMETER))
	})
})
