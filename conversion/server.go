package convert

import (
	"context"
	"fmt"

	pb "gitlab.com/linuxfreak003/go-convert/pb"
)

type Server struct {
	ConversionGraph *Graph
}

func NewServer() *Server {
	return &Server{
		ConversionGraph: NewGraph(),
	}
}

func (s *Server) Convert(ctx context.Context, req *pb.ConvertRequest) (*pb.ConvertResponse, error) {
	value, err := s.ConversionGraph.Convert(req.GetValue(), req.GetUnitIn(), req.GetUnitOut())
	if err != nil {
		return nil, fmt.Errorf("could not convert: %v", err)
	}

	return &pb.ConvertResponse{
		Value: value,
		Unit:  req.GetUnitOut(),
	}, nil
}

func (s *Server) CreateConversion(ctx context.Context, req *pb.Conversion) (*pb.Conversion, error) {
	s.ConversionGraph.CreateEdge(&Edge{
		From:   req.GetStart(),
		To:     req.GetEnd(),
		Weight: req.GetMultiplier(),
	})
	return nil, fmt.Errorf("unimplemented")
}
func (s *Server) ListConversions(ctx context.Context, req *pb.ListConversionsRequest) (*pb.ListConversion, error) {
	return nil, fmt.Errorf("unimplemented")
}
func (s *Server) UpdateConversion(ctx context.Context, req *pb.Conversion) (*pb.Conversion, error) {
	return nil, fmt.Errorf("unimplemented")
}
func (s *Server) DeleteConversion(ctx context.Context, req *pb.Conversion) (*pb.Conversion, error) {
	return nil, fmt.Errorf("unimplemented")
}
func (s *Server) GetConversion(ctx context.Context, req *pb.Conversion) (*pb.Conversion, error) {
	return nil, fmt.Errorf("unimplemented")
}
