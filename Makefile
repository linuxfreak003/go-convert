all: build

build: generate
	go build

test:
	ginkgo -r ./

generate:
	protoc --go_out=plugins=grpc:. pb/*.proto

clean:
	@rm -f go-convert

deepclean: clean
	@rm -f pb/convert.pb.go
